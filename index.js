const express = require('express')
const app = express()
var http = require('http').Server(app);
var io = require('socket.io')(http);
const LISTEN_PORT = process.env.PORT || 3000;


var options = {
  dotfiles: 'ignore',
  etag: false,
  extensions: ['htm', 'html', 'js', 'jpg'],
  index: false,
  maxAge: '1d',
  redirect: false,
  setHeaders: function (res, path, stat) {
    res.set('x-timestamp', Date.now())
  }
}

app.use(express.static('public', options))


app.get("/manifest.appcache", function(req, res){
  res.header("Content-Type", "text/cache-manifest");
  res.end("CACHE MANIFEST");
});

app.get('/',function( req,res){
  res.sendFile(__dirname + '/index.html');
});
app.get('/modelo.html',function( req,res){
  res.sendFile(__dirname + '/modelo.html');
});
app.get('/display',function( req,res){
  res.sendFile(__dirname + '/modelo.html');
});

app.get('/admin',function( req,res){
  res.sendFile(__dirname + '/admin.html');
});

io.on('connection',function(socket){
  console.log('a user connected');
  socket.on('disconnect',function(){
    console.log('user disconnected')
  });

  socket.on('chat message',function(msg){
    console.log('message: ' + msg);
    io.emit('chat message', msg);

    io.emit('slide add', { hello: 'world' });
  });


  socket.on('slide add',function(data){
    io.emit('slide add',data);
  });

});


http.listen(LISTEN_PORT,function(){
  console.log('listening on port '+LISTEN_PORT);
});
